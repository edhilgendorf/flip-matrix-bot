# flip-matrix-bot

Copyright © 2023 Andrew "HER0" Conrad

flip-matrix-bot is a [Matrix](https://matrix.org) bot for the [Friendly Linux Players](https://friendlylinuxplayers.org) community.

One of the primary goals is to provide a CLI-like interface for anyone in the community to schedule gaming events. Join [#flip:flip.earth](https://matrix.to/#/#flip:flip.earth) and send a message starting with `!f` to get started with using it. Alternately, you can see it being tested in [#bots:flip.earth](https://matrix.to/#/#bots:flip.earth). Discussion on the bot itself belongs in either [the issues](https://gitlab.com/FriendlyLinuxPlayers/flip-matrix-bot/-/issues) or [#flip-matrix-bot:flip.earth](https://matrix.to/#/#flip-matrix-bot:flip.earth).

This code is freely available on
[GitLab](https://gitlab.com/FriendlyLinuxPlayers/flip-matrix-bot) under the terms of
the GNU GPL version 3. See `LICENSE.md` for details.

By participating in this community, you agree to abide by the terms of our
[Code of Conduct](https://FriendlyLinuxPlayers.org/conduct).

## Installation

### Requirements

On Linux, you need OpenSSL.

### Minimum Rust version

flip-matrix-bot is built against the latest stable version of Rust. Older versions are untested, and may not work.

### Build instructions

```
cargo build
```

## Basic usage

### Starting the bot

See the following example for starting the bot:

```
flip-matrix-bot -h https://matrix.your.homeserver -u username -p hunter2 -r #example-room:your.homeserver -s !abcdefghijklmnop:your.homeserver
```

Of note, `-s` specifies the room that will be used for storing data. `flip-matrix-bot` stores data as custom events in a Matrix room, as opposed to writing local files or connecting to a database.

Further usage information is documented using the `help` subcommand or `--help` flag.

### Interacting with the bot

In your Matrix room, you can interact with the bot by sending `!f`. This works similarly to a typical CLI application.

Help messages and replies to commands with bad input will be sent in a thread. You can reply within the thread when forming your commands, so as to reduce clutter in the room if mistakes are made, or if you need more help output.

#### Events

The biggest feature of `flip-matrix-bot` is the `events` command.

The `add` subcommand can be used to create a new event:

```
!f events add "OpenRCT2" https://openrct2.org "Sunday 13:00" US/Eastern
```

There will be a reminder sent to the room an hour before and at the scheduled event time.

Upcoming events can be `modify`ed using the command that is included in the output when scheduing a
new event. For example, if the timezone was forgotten when scheduling, you may change the start
time to the correct timezone like so:

```
!f events modify 53b89 -T Pacific/Auckland
```

Not specifying a subcommand will provide a list of upcoming events (this example uses the `e` alias):

```
!f e
```

Listing events will provide an ID that can be used to cancel an event:

```
!f events cancel e5dff
```

#### Information

There are various `information` subcommands that reply with useful data. For example, you can get a link to the GitLab repository with the following:

```
!f information source
```

#### Further usage

For more details on how to use this, try the `help` commands, or add the `--help` parameter to any command.
