use chrono::Duration;
use icalendar::{Alarm, Calendar as Cal, Component, Event as CalEvent, EventLike, EventStatus};
use std::{path::PathBuf, sync::Arc};
use tokio::{fs::File, io::AsyncWriteExt, sync::Mutex};
use url::Url;

use crate::data::FlipEventContent;
use crate::storage::Store;

#[derive(Clone, Debug)]
pub struct Calendar {
    pub url: Option<Url>,
    calendar: Arc<Mutex<Cal>>,
    file_path: PathBuf,
}

impl Calendar {
    pub fn new(file_path: PathBuf, url: Option<Url>) -> Calendar {
        let mut inner = Cal::new();
        inner.name("FLiP gaming events");
        inner.description(
            "Gaming events scheduled by members of the Friendly Linux Players community.",
        );

        Calendar {
            url,
            calendar: Mutex::new(inner).into(),
            file_path,
        }
    }

    pub async fn add_event(&self, event: FlipEventContent, write: bool) {
        let status = if event.cancelled {
            EventStatus::Cancelled
        } else {
            EventStatus::Confirmed
        };

        let description = format!(
            "{} has volunteered to host an event for the following game: {}.\n\n\
            For more information, join the Matrix room:\n\
            {}\n\
            Then send this message to query the bot:\n\
            !f events --id {}",
            event.host,
            event.game,
            event.mx_room_permalink(),
            event.id_hex()
        );

        let alarm = Alarm::display(
            &format!("{} starts in 1 hour.", event.name_or_game()),
            -Duration::hours(1),
        );

        let cal_ev = CalEvent::new()
            .summary(&event.name_or_game())
            .description(&description)
            .starts(event.time)
            .ends(event.time + Duration::hours(1))
            .location(&event.meeting_place)
            .url(&event.mx_msg_permalink().to_string())
            .timestamp(event.mx_time)
            .uid(&event.id_hex())
            .sequence(event.generation.into())
            .status(status)
            .alarm(alarm)
            .done();
        self.calendar.lock().await.push(cal_ev);

        if write {
            self.write().await;
        };
    }

    pub async fn populate_events(&self, store: &Store) {
        let option = store.get_events(None).await;
        if let Some(events) = option {
            for e in events {
                self.add_event(e, false).await;
            }
        }
        self.write().await;
    }

    async fn write(&self) {
        let mut file = File::create(self.file_path.clone()).await.unwrap();
        file.write_all_buf(&mut self.calendar.lock().await.to_string().as_bytes())
            .await
            .unwrap();
    }
}
