use chrono::{Duration, Utc};
use chrono_humanize::HumanTime;
use matrix_sdk::room::Joined;
use std::{collections::HashMap, sync::Arc};
use tokio::{sync::Mutex, task::JoinSet, time::sleep};

use crate::data::FlipEventContent;
use crate::flip::interface::sender::send;
use crate::storage::Store;

#[derive(Clone, Debug, Default)]
pub struct Events {
    tasks: Arc<Mutex<HashMap<u64, JoinSet<()>>>>,
}

fn event_fmt(event: FlipEventContent) -> String {
    let name = match event.name {
        Some(n) => format!("**Event:** {n}  \n"),
        None => "".to_string(),
    };

    format!(
        "{name}**Game:** [{}]({})  \n\
        **Host:** [{}]({})  \n\
        **Meeting place:** {}",
        event.game,
        event.game_url,
        event.host,
        event.host.matrix_to_uri(),
        event.meeting_place,
    )
}

impl Events {
    pub async fn schedule(&self, event: FlipEventContent, room: Joined) {
        if event.cancelled || event.passed {
            return;
        };
        let mut set = JoinSet::new();

        let id = event.id;
        let time = event.time - Utc::now();

        let hour = Duration::hours(1);
        let reminder_time = if time > hour { time - hour } else { time / 2 };

        let msg = event_fmt(event);
        let happening = format!("### Event starting now\n\n{msg}");
        let on_happening = send(happening, room.clone(), true);
        let h_timer = async move {
            sleep(time.to_std().unwrap()).await;
            on_happening.await;
        };
        set.spawn(h_timer);

        let ht = HumanTime::from(time - reminder_time);
        let reminder = format!("### Event starting {ht}\n\n{msg}");
        let on_reminder = send(reminder, room, true);
        let r_timer = async move {
            sleep(reminder_time.to_std().unwrap()).await;
            on_reminder.await;
        };
        set.spawn(r_timer);

        self.tasks.lock().await.insert(id, set);
    }

    pub async fn delete(&self, id: &u64) {
        self.tasks.lock().await.remove(id);
    }

    pub async fn schedule_from_store(&self, store: &Store, room: Joined) {
        let opt = store.get_events(None).await;
        let Some(events) = opt else {
            return;
        };

        for e in events {
            self.schedule(e, room.clone()).await;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use matrix_sdk::ruma::room_id;
    use serde_json::{from_value, json};
    use url::Url;

    fn dummy_event() -> FlipEventContent {
        let event_json = json!({
          "content": {
            "body": "Dummy message",
            "msgtype": "m.text"
          },
          "event_id": "$1231231231231230IWLui:matrix.org",
          "origin_server_ts": 1674868033938i64,
          "sender": "@user:matrix.org",
          "type": "m.room.message"
        });
        FlipEventContent::new(
            /* name: */ Some("Event name".to_owned()),
            /* game: */ "Game".to_owned(),
            /* game_url: */ Url::parse("https://example.net").unwrap(),
            /* time_string: */ "2023-02-04 22:00".to_owned(),
            /* timezone: */ chrono_tz::UTC,
            /* meeting_place: */ "Middle of nowhere".to_owned(),
            /* ev: */ &from_value(event_json).unwrap(),
            /* mx_room: */ room_id!("!somewhere:example.org").to_owned(),
            /* mx_route: */ Default::default(),
        )
        .unwrap()
    }

    #[test]
    fn event_formatting() {
        let event = dummy_event();
        assert_eq!(
            event_fmt(event),
            "**Event:** Event name  \n\
            **Game:** [Game](https://example.net/)  \n\
            **Host:** [@user:matrix.org](https://matrix.to/#/%40user%3Amatrix.org)  \n\
            **Meeting place:** Middle of nowhere"
        );
    }

    #[test]
    fn event_formatting_no_name() {
        let mut event = dummy_event();
        event.name = Default::default();
        assert_eq!(
            event_fmt(event),
            "**Game:** [Game](https://example.net/)  \n\
            **Host:** [@user:matrix.org](https://matrix.to/#/%40user%3Amatrix.org)  \n\
            **Meeting place:** Middle of nowhere"
        );
    }
}
