// Parse chat messages

use anyhow::Error;
use chrono::Local;
use chrono_tz::Tz;
use clap::{error::ErrorKind, ArgGroup, Args, CommandFactory, Parser, Subcommand};
use interim::{parse_date_string, Dialect};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::{
    events::{room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent},
    OwnedEventId,
};
use shell_words;
use url::Url;

use crate::context::EventHelper;

mod events;
mod information;
pub mod sender;

#[derive(Parser)]
#[command(author = "@HER0:matrix.org", version, about, long_about = None)]
struct Chat {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Schedule, list, and manage community gaming events
    ///
    /// With no subcommand, lists upcoming events.
    Events(EArgs),
    /// Display information on the FLiP community
    ///
    /// Use subcommands to get details about a specific topic.
    Information(IArgs),
}

#[derive(Args)]
#[command(visible_alias = "e", args_conflicts_with_subcommands = true)]
#[command(group(
            ArgGroup::new("by")
                .args(["id", "mx_id"]),
        ))]
pub struct EArgs {
    /// Display events in long format
    #[arg(short, long)]
    long: bool,
    /// Display events matching an ID
    #[arg(short, long)]
    id: Option<String>,
    /// Display event created by the message with a given Matrix event ID
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
    #[command(subcommand)]
    ecommands: Option<Ecoms>,
}

#[derive(Subcommand)]
enum Ecoms {
    /// Schedule a new event
    Add(AddEvents),
    /// Modify an upcoming event
    Modify(ModEvents),
    /// Cancel an upcoming event
    Cancel(CanEvents),
}

#[derive(Args)]
pub struct AddEvents {
    /// The title of this event (e.g. "Virtual board game night")
    #[arg(short, long)]
    name: Option<String>,
    /// The game to be played
    game: String,
    /// Link to the game (for example, the Steam store page)
    game_url: Url,
    /// Time that the event starts (e.g. "2017-06-30 15:40" or "Friday 6pm")
    #[arg(value_parser = is_fuzzy_time)]
    time: String,
    /// Timezone of the scheduled time (e.g. "US/Central", "CET", or "Etc/GMT+4")
    #[arg(default_value = "UTC")]
    timezone: Tz,
    /// Where everyone can talk during the event (like a Mumble channel or Matrix room)
    #[arg(
        short,
        long,
        default_value = "the Event channel on `mumble.flip.earth`"
    )]
    meeting_place: String,
}

#[derive(Args)]
#[command(visible_alias = "mod")]
#[command(group(
            ArgGroup::new("by")
                .required(true)
                .args(["id", "mx_id"]),
        ))]
#[command(group(
            ArgGroup::new("field")
                .required(true)
                .args(["name", "game", "game_url", "time", "timezone", "meeting_place", "uncancel"])
                .multiple(true),
        ))]
pub struct ModEvents {
    /// Modify by event ID, as shown when listing events
    id: Option<String>,
    /// Modify by the Matrix event ID of the command which created the event
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
    /// Set the title of the event
    #[arg(short, long)]
    name: Option<String>,
    /// Set the game to be played
    #[arg(short, long)]
    game: Option<String>,
    /// Set the game URL
    #[arg(short = 'u', long)]
    game_url: Option<Url>,
    /// Set the event start time (e.g. "2017-06-30 15:40" or "Friday 6pm")
    #[arg(short, long, value_parser = is_fuzzy_time)]
    time: Option<String>,
    /// Set the timezone for the event start time (e.g. "US/Central", "CET", or "Etc/GMT+4")
    ///
    /// Note that changing this to a timezone with a different offset without changing the time
    /// will change the start time of the event.
    #[arg(short = 'T', long)]
    timezone: Option<Tz>,
    /// Set where everyone can talk during the event
    #[arg(short = 'M', long)]
    meeting_place: Option<String>,
    /// Revert the cancelling of a cancelled event.
    #[arg(short = 'C', long)]
    uncancel: bool,
}

#[derive(Args)]
#[command(visible_alias = "can")]
#[command(group(
            ArgGroup::new("by")
                .required(true)
                .args(["id", "mx_id"]),
        ))]
pub struct CanEvents {
    /// Cancel by event ID, as shown when listing events
    id: Option<String>,
    /// Cancel by the Matrix event ID of the command which created the event
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
}

#[derive(Args)]
#[command(visible_alias = "i", visible_alias = "info")]
pub struct IArgs {
    #[command(subcommand)]
    icommands: Option<Icoms>,
}

#[derive(Subcommand)]
enum Icoms {
    /// Get a link to the website
    Website,
    /// Get a link to the Code of Conduct
    Conduct,
    /// Get information on our Matrix space
    Space,
    /// Get information on community events
    #[command(visible_alias = "calendar")]
    Events,
    /// Get information about our Mumble server
    #[command(visible_alias = "voice")]
    Mumble,
    /// Get information about our Steam group
    Steam,
    /// Get a link to the flip-matrix-bot source code
    Source,
    /// Get a link to our GitLab organization
    Gitlab,
    /// Get information on using threads with the bot
    BotThreads,
    /// Get information on guests in the community
    Guests,
}

fn is_fuzzy_time(s: &str) -> Result<String, Error> {
    let _ = parse_date_string(s, Local::now(), Dialect::Us)?;
    Ok(s.to_owned())
}

pub async fn process(
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    command: String,
    joined_room: Joined,
    state: EventHelper,
) {
    let Ok(param_itr) = shell_words::split(&command) else {
        let mut cmd = Chat::command().bin_name("!f");
        let err = cmd.error(ErrorKind::Format, "Unable to split command into parameters. Did you miss a quote?");
        sender::reply_error(err.into(), ev, joined_room).await;
        return
    };

    let r = Chat::try_parse_from(param_itr);

    match r {
        Err(err) => sender::reply_error(err.into(), ev, joined_room).await,
        Ok(args) => match args.command {
            Commands::Events(e) => events::run(e, ev, joined_room, state).await,
            Commands::Information(i) => information::run(i, ev, joined_room, state).await,
        },
    }
}
