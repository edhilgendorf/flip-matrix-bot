use anyhow::Error;
use clap::error::Error as ClapError;
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::events::{
    room::message::{ForwardThread, ReplyWithinThread, RoomMessageEventContent},
    OriginalSyncMessageLikeEvent,
};

fn notice(msg: String, markdown: bool) -> RoomMessageEventContent {
    if markdown {
        RoomMessageEventContent::notice_markdown(msg)
    } else {
        RoomMessageEventContent::notice_plain(msg)
    }
}

/// Reply with an error message in a thread (if supported)
pub async fn reply_error(
    e: Error,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
) {
    let msg = match e.downcast::<ClapError>() {
        Ok(v) => v.render().to_string(),
        Err(e) => e.to_string(),
    };

    let full_ev = &ev.into_full_event(joined_room.room_id().to_owned());
    let content = notice(msg, false).make_for_thread(full_ev, ReplyWithinThread::No);
    joined_room.send(content, None).await.unwrap();
}

/// Reply with a message
pub async fn reply(
    msg: String,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    markdown: bool,
) {
    let full_ev = &ev.into_full_event(joined_room.room_id().to_owned());
    let content = notice(msg, markdown).make_reply_to(full_ev, ForwardThread::No);
    joined_room.send(content, None).await.unwrap();
}

/// Send a message
pub async fn send(msg: String, joined_room: Joined, markdown: bool) {
    let content = notice(msg, markdown);
    joined_room.send(content, None).await.unwrap();
}
