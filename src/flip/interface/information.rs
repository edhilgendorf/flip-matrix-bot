use matrix_sdk::room::Joined;
use matrix_sdk::ruma::events::{
    room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent,
};

use super::sender::reply;
use super::Icoms;
use crate::context::EventHelper;

fn general() -> String {
    "[Friendly Linux Players (FLiP)](https://friendlylinuxplayers.org/) is an inclusive online gaming community of Linux enthusiasts. \
    If you have interest in the community, and can stick to our [Code of Conduct](https://friendlylinuxplayers.org/conduct), you are welcome here! Try `!f events` to see upcoming gaming events.  \n\
    **Matrix space**: [FLiP Community](https://matrix.to/#/#community:flip.earth)  \n\
    **Mumble server**: `mumble.flip.earth`  \n\
    **Steam group**: [Friendly Linux Players](https://steamcommunity.com/groups/FriendlyLinuxPlayers)".to_string()
}

fn website() -> String {
    "https://friendlylinuxplayers.org".to_string()
}

fn conduct() -> String {
    "https://friendlylinuxplayers.org/conduct".to_string()
}

fn space() -> String {
    "Join the community space to better discover and organize all of our official rooms: \
    [FLiP Community](https://matrix.to/#/#community:flip.earth)"
        .to_string()
}

fn events(state: EventHelper) -> String {
    let mut m = "Anyone in the community can volunteer to host a gaming event. To list or create \
                 events, use the `events` subcommand. Try `!f help events` to see how to use this."
        .to_string();
    if let Some(feed_url) = state.calendar.url {
        m = format!(
            "{m}\n\nSubscribe to the calendar feed: [{0}]({0})",
            feed_url
        );
    };

    m
}

fn mumble() -> String {
    "Join our [Mumble](https://www.mumble.info/) server for voice chat: \
    `mumble://mumble.flip.earth`  \n\
    Note that registering yourself allows you to create temporary channels."
        .to_string()
}

fn steam() -> String {
    "Join the [Steam group](https://steamcommunity.com/groups/FriendlyLinuxPlayers). \
    Note that joining is strictly optional for participating in the community."
        .to_string()
}

fn source() -> String {
    "https://gitlab.com/FriendlyLinuxPlayers/flip-matrix-bot".to_string()
}

fn gitlab() -> String {
    "https://gitlab.com/FriendlyLinuxPlayers".to_string()
}

fn bot_threads() -> String {
    "If you are using a Matrix client which supports threads, and the bot \
    replies in a thread, please send further responses within the thread, so as \
    to reduce clutter in the room when crafting your command."
        .to_string()
}

fn guests() -> String {
    "The numbered users in this room are guest users. These are dynamically \
    created when someone tries to access the room without being signed in to a \
    Matrix account, lowering the barrier to participate.\n\n\
    Eventually, these may be removed automatically."
        .to_string()
}

pub async fn run(
    args: super::IArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let msg = match args.icommands {
        None => general(),
        Some(Icoms::Website) => website(),
        Some(Icoms::Conduct) => conduct(),
        Some(Icoms::Space) => space(),
        Some(Icoms::Events) => events(state),
        Some(Icoms::Mumble) => mumble(),
        Some(Icoms::Steam) => steam(),
        Some(Icoms::Source) => source(),
        Some(Icoms::Gitlab) => gitlab(),
        Some(Icoms::BotThreads) => bot_threads(),
        Some(Icoms::Guests) => guests(),
    };
    reply(msg, ev, joined_room, true).await;
}
