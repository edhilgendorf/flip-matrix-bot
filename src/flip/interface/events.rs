// Events subcommand

use matrix_sdk::room::Joined;
use matrix_sdk::ruma::events::{
    room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent,
};

use super::Ecoms;
use crate::context::EventHelper;

mod add;
mod list;
mod modify;

pub async fn run(
    args: super::EArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    match args.ecommands {
        None => list::run(args, ev, joined_room, state).await,
        Some(Ecoms::Add(a)) => add::run(a, ev, joined_room, state).await,
        Some(Ecoms::Modify(a)) => modify::run(a, ev, joined_room, state).await,
        Some(Ecoms::Cancel(a)) => modify::cancel(a, ev, joined_room, state).await,
    }
}
