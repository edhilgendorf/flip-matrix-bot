use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::{
    events::{room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent},
    OwnedEventId,
};

use super::super::{sender, EArgs};
use crate::context::EventHelper;
use crate::data::FlipEventContent;
use crate::storage::Store;

async fn list_fmt(event: FlipEventContent, long: bool, joined_room: Joined) -> String {
    let (id, time, place, suffix) = if long {
        let id = event.id_hex();
        let time = event.time_tz().to_rfc3339();
        let place = format!(" in {}", event.meeting_place,);
        let suffix = format!(
            "    Scheduled in [{}]({}) on [{}]({}).\n\n",
            event.mx_room,
            event.mx_room_permalink(),
            event.mx_time_tz(),
            event.mx_msg_permalink(),
        );
        (id, time, place, suffix)
    } else {
        let id = format!("{:.5}", event.id_hex());
        let time = event
            .time_tz()
            .format("%a, %b %-d %-I:%M%P UTC%:z")
            .to_string();
        let place = "".to_string();
        let suffix = "\n".to_string();
        (id, time, place, suffix)
    };

    let (tense, hs) = match event.passed {
        true => (Tense::Past, ""),
        false => (Tense::Future, " from now"),
    };
    let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, tense) + hs;

    let cancelled = match event.cancelled {
        true => "(Cancelled) - Would have been",
        false => "-",
    };

    let host = joined_room.get_member(&event.host).await.unwrap().unwrap();
    let display_name = host.display_name().unwrap();

    format!(
        "`{}` - **{}** {} {}  \n    Game: {} - Hosted by [{}]({}) on {}{}.  \n{}",
        id,
        event.name_or_game(),
        cancelled,
        ht,
        event.md_game(),
        display_name,
        event.host.matrix_to_uri(),
        time,
        place,
        suffix,
    )
}

pub async fn upcoming(
    args: EArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let Some(events) = state.store.get_events(Some(10)).await else {
        sender::reply("No events found.".to_string(), ev, joined_room, false).await;
        return
    };

    let mut count: u8 = 0;
    let mut m = String::new();
    for e in events {
        if e.passed || e.cancelled {
            continue;
        };

        count += 1;
        let msg = list_fmt(e, args.long, joined_room.clone()).await;
        m = format!("{m}- {msg}");
    }
    m = match count {
        0 => "No upcoming events found. Try scheduling a new one with `!f events add`.".to_string(),
        1 => format!("One upcoming event:\n\n{m}"),
        _ => format!("Upcoming events:\n\n{m}"),
    };

    if let Some(feed_url) = state.calendar.url {
        m = format!(
            "{m}\n\nSubscribe to the calendar feed: [{0}]({0})",
            feed_url
        );
    };

    sender::reply(m.to_string(), ev, joined_room, true).await;
}

pub async fn by_id(
    id: String,
    args: EArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    store: Store,
) {
    let Some(events) = store.get_event_by_id(&id).await else {
        sender::reply(format!("No event with ID `{id}` found."), ev, joined_room, true).await;
        return
    };

    let m = if events.len() == 1 {
        format!(
            "Event found:\n\n- {}",
            list_fmt(events[0].clone(), args.long, joined_room.clone()).await
        )
    } else {
        let mut msg =
            format!("Disambiguation needed: multiple events found matching ID `{id}`:\n\n");
        for e in events {
            let li = list_fmt(e, true, joined_room.clone()).await;
            msg = format!("{msg}- {li}");
        }
        msg
    };

    sender::reply(m.to_string(), ev, joined_room, true).await;
}

pub async fn by_mx_id(
    mx_id: OwnedEventId,
    args: EArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    store: Store,
) {
    let Some(event) = store.get_event_by_mx_id(mx_id.clone()).await else {
        sender::reply(format!("No Matrix message with ID `{mx_id}` found."), ev, joined_room, true).await;
        return
    };

    let m = format!(
        "Event found:\n\n- {}",
        list_fmt(event, args.long, joined_room.clone()).await
    );

    sender::reply(m.to_string(), ev, joined_room, true).await;
}

pub async fn run(
    args: EArgs,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let (oid, omx_id) = (args.id.clone(), args.mx_id.clone());
    match (oid, omx_id) {
        (Some(id), _) => by_id(id, args, ev, joined_room, state.store).await,
        (_, Some(mx_id)) => by_mx_id(mx_id, args, ev, joined_room, state.store).await,
        _ => upcoming(args, ev, joined_room, state).await,
    };
}
