use anyhow::anyhow;
use chrono::{Duration, Offset};
use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::{
    events::{room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent},
    OwnedEventId,
};

use super::super::{sender, CanEvents, ModEvents};
use crate::context::EventHelper;
use crate::data::{parse_time, FlipEventContent};

async fn get_event(
    oid: Option<String>,
    omx_id: Option<OwnedEventId>,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: &EventHelper,
) -> Option<FlipEventContent> {
    let event = match (oid, omx_id) {
        (Some(id), _) => {
            let opt = state.store.get_event_by_id(&id).await;
            let Some(mut events) = opt else {
                sender::reply(format!("No event with ID `{id}` found."), ev, joined_room, true).await;
                return None;
            };
            if events.len() > 1 {
                sender::reply(format!("Disambiguation needed: multiple events found matching ID `{id}`.  \nTry running `!f events {id}` to get the full ID."), ev, joined_room, true).await;
                return None;
            }
            events.pop().unwrap()
        }
        (_, Some(mx_id)) => {
            let Some(event) = state.store.get_event_by_mx_id(mx_id.clone()).await else {
        sender::reply(format!("No Matrix message with ID `{mx_id}` found."), ev, joined_room, true).await;
        return None;
    };
            event
        }
        _ => unreachable!(),
    };
    Some(event)
}

pub async fn cancel(
    args: CanEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let oevent = get_event(
        args.id.clone(),
        args.mx_id.clone(),
        ev.clone(),
        joined_room.clone(),
        &state,
    )
    .await;
    let Some(mut event) = oevent else {
        return;
    };

    let requester = joined_room.get_member(&ev.sender).await.unwrap().unwrap();
    let moderator = requester.normalized_power_level() >= 50;
    let scheduler = event.host == ev.sender;

    if moderator || scheduler {
        if event.cancelled {
            let msg = format!("Event `{}` has already been cancelled.", event.id_hex());
            sender::reply(msg, ev, joined_room, true).await;
            return;
        };
        if event.passed {
            let msg = format!("Event `{}` has already taken place.", event.id_hex());
            sender::reply(msg, ev, joined_room, true).await;
            return;
        };
        let id = event.id;
        event.generation += 1;
        event.cancelled = true;
        let msg = format!("Event `{}` cancelled.", event.id_hex());

        state
            .store
            .update_event_by_full_id(event, id)
            .await
            .unwrap();
        state.reminders.delete(&id).await;

        sender::reply(msg, ev, joined_room, true).await;
    } else {
        let err = anyhow!(
            "You must be the event host or a room moderator to cancel event `{}`",
            event.id_hex()
        );
        sender::reply_error(err, ev, joined_room).await;
    };
}

pub async fn run(
    args: ModEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let oevent = get_event(
        args.id.clone(),
        args.mx_id.clone(),
        ev.clone(),
        joined_room.clone(),
        &state,
    )
    .await;
    let Some(mut event) = oevent else {
        return;
    };

    if event.host == ev.sender {
        if event.passed {
            let msg = format!("Event `{}` has already taken place.", event.id_hex());
            sender::reply(msg, ev, joined_room, true).await;
            return;
        };
        if event.cancelled && !args.uncancel {
            let msg = format!(
                "Event `{}` has been cancelled. Consider adding `--uncancel` to revert this.",
                event.id_hex()
            );
            sender::reply(msg, ev, joined_room, true).await;
            return;
        };

        let mut changed = false;
        let mut tz_diff = None;

        if args.name != event.name {
            changed = true;
            event.name = args.name;
        };
        if let Some(game) = args.game {
            if game != event.game {
                changed = true;
                event.game = game;
            };
        };
        if let Some(game_url) = args.game_url {
            if game_url != event.game_url {
                changed = true;
                event.game_url = game_url;
            };
        };
        if let Some(timezone) = args.timezone {
            if timezone != event.timezone {
                changed = true;

                let time = event.time;
                let old_offset_s = time
                    .with_timezone(&event.timezone)
                    .offset()
                    .fix()
                    .local_minus_utc();
                let offset_s = time
                    .with_timezone(&timezone)
                    .offset()
                    .fix()
                    .local_minus_utc();
                let diff = offset_s - old_offset_s;
                if diff != 0 {
                    tz_diff = Some(diff);
                };

                event.timezone = timezone;
            };
        };
        if let Some(meeting_place) = args.meeting_place {
            if meeting_place != event.meeting_place {
                changed = true;
                event.meeting_place = meeting_place;
            };
        };
        if args.uncancel && event.cancelled {
            changed = true;
            event.cancelled = false;
        };

        match args.time {
            Some(time_string) => {
                let times = parse_time(time_string, event.timezone, ev.origin_server_ts);

                let time = match times {
                    Ok((t, _)) => t,
                    Err(e) => {
                        sender::reply_error(e, ev, joined_room).await;
                        return;
                    }
                };

                if time != event.time {
                    changed = true;
                    event.time = time;
                }
            }
            None => {
                if let Some(diff) = tz_diff {
                    let diff_duration = Duration::seconds(diff.into());
                    event.time -= diff_duration;
                }
            }
        };

        if changed {
            event.generation += 1;
        } else {
            let msg = format!("Event `{}` has not been changed, as none of the provided values were different from the current version of the event.", event.id_hex());
            sender::reply(msg, ev, joined_room, true).await;
            return;
        };

        let id = event.id;
        state
            .store
            .update_event_by_full_id(event.clone(), id)
            .await
            .unwrap();
        state.reminders.delete(&id).await;
        state
            .reminders
            .schedule(event.clone(), joined_room.clone())
            .await;

        let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, Tense::Future);
        let name = match event.name {
            Some(ref n) => format!("**Name:** {n}  \n"),
            None => "".to_string(),
        };

        let msg = format!(
            "### Event modified:\n\n\
            {}**Game:** {}  \n\
            **Time:** {} ({})  \n\
            **Meeting place:** {}  \n\
            **ID:** `{}`",
            name,
            event.md_game(),
            event.time_tz().to_rfc2822(),
            ht,
            event.meeting_place,
            event.id_hex(),
        );
        sender::reply(msg, ev, joined_room, true).await;
    } else {
        let err = anyhow!(
            "You must be the event host to modify event `{}`",
            event.id_hex()
        );
        sender::reply_error(err, ev, joined_room).await;
    };
}
