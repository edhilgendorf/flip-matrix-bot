// Add events subcommand

use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::events::{
    room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent,
};

use super::super::{sender, AddEvents};
use crate::context::EventHelper;
use crate::data::FlipEventContent as Event;

async fn creation_success(event: Event, joined_room: Joined, state: EventHelper) {
    state.add(event.clone(), joined_room.clone()).await;

    let name = match event.name {
        Some(ref n) => format!("**Name:** {n}  \n"),
        None => "".to_string(),
    };

    let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, Tense::Future);

    // The member is within an Option, which is with a Result. Hence, two unwraps.
    let host = joined_room.get_member(&event.host).await.unwrap().unwrap();
    let display_name = host.display_name().unwrap();
    let extra_thx = match host.normalized_power_level() {
        0..=49 => ", and contributing to what makes FLiP special!",
        _ => "!",
    };

    let msg = format!(
        "### Event scheduled:\n\n\
        {}**Game:** {}  \n\
        **Time:** {} ({})  \n\
        **Meeting place:** {}\n\n\
        Thanks, [{}]({}), for [volunteering to host]({}){} \
        Use `!f events modify {}` to change these values.",
        name,
        event.md_game(),
        event.time_tz().to_rfc2822(),
        ht,
        event.meeting_place,
        display_name,
        event.host.matrix_to_uri(),
        event.mx_msg_permalink(),
        extra_thx,
        event.id_hex(),
    );
    sender::send(msg.to_string(), joined_room, true).await;
}

pub async fn run(
    args: AddEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) {
    let event = Event::new(
        args.name,
        args.game,
        args.game_url,
        args.time,
        args.timezone,
        args.meeting_place,
        &ev,
        joined_room.room_id().to_owned(),
        joined_room.route().await.unwrap(),
    );

    match event {
        Ok(v) => creation_success(v, joined_room, state).await,
        Err(e) => {
            sender::reply_error(e, ev, joined_room).await;
        }
    };
}
