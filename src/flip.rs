// Determine whether incoming messages are ones we have interest in

use matrix_sdk::ruma::events::{
    room::message::{MessageType, RoomMessageEventContent},
    OriginalSyncMessageLikeEvent,
};
use matrix_sdk::{event_handler::Ctx, room::Room};

use crate::context::EventHelper;

pub mod interface;

pub async fn event_handler(
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    room: Room,
    state: Ctx<EventHelper>,
) {
    if let Room::Joined(joined) = room {
        let MessageType::Text(ref text_content) = ev.content.msgtype else {
            return;
        };

        joined.read_receipt(&ev.event_id).await.unwrap();

        let body = &text_content.body;
        if body.starts_with("!f") {
            let owned = body.to_owned();
            let inner = state.0;
            interface::process(ev, owned, joined, inner).await;
        }
    } else {
        let room_id = room.room_id();
        panic!("Message received from room {room_id}, but membership in room has been revoked.");
    }
}
