use matrix_sdk::room::Joined;

use crate::calendar::Calendar;
use crate::data::FlipEventContent;
use crate::reminders::Events;
use crate::storage::Store;

#[derive(Clone, Debug)]
pub struct EventHelper {
    pub store: Store,
    pub reminders: Events,
    pub calendar: Calendar,
}

impl EventHelper {
    pub async fn add(self, event: FlipEventContent, joined_room: Joined) {
        self.store.store_event(event.clone()).await;
        self.reminders.schedule(event.clone(), joined_room).await;
        self.calendar.add_event(event, true).await;
    }
}
