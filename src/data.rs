use anyhow::Error;
use chrono::{
    offset::{Offset, TimeZone as OTz},
    DateTime, Duration, LocalResult, Utc,
};
use chrono_tz::Tz;
use clap::error::{Error as ClapError, ErrorKind};
use interim::{parse_date_string, Dialect};
use matrix_sdk::ruma::{
    events::{
        macros::EventContent,
        room::message::{deserialize_relation, Relation, RoomMessageEventContent},
        OriginalSyncMessageLikeEvent,
    },
    MatrixToUri, MilliSecondsSinceUnixEpoch, OwnedEventId, OwnedRoomId, OwnedServerName,
    OwnedUserId,
};
use seahash::hash;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};
use url::Url;

/// Parse the time from a given string and timezone, relative to a unix timestamp.
pub fn parse_time(
    time_string: String,
    timezone: Tz,
    mx_unix_time: MilliSecondsSinceUnixEpoch,
) -> Result<(DateTime<Utc>, DateTime<Utc>), Error> {
    let mx_time_opt = Utc.timestamp_millis_opt(mx_unix_time.0.into());
    let LocalResult::Single(mx_time) = mx_time_opt else {
            return Err(Error::new::<ClapError>(ClapError::raw(ErrorKind::Io, "Unix timestamp from Matrix message is out of range.")))
        };
    let local_mx_time = mx_time.with_timezone(&timezone);
    let local_offset = local_mx_time.offset().fix();
    let fixed_mx_time = local_mx_time.with_timezone(&local_offset);

    let time_fixed = parse_date_string(&time_string, fixed_mx_time, Dialect::Us)?;
    let time = time_fixed.with_timezone(&Utc);

    if time - Utc::now() <= Duration::zero() {
        return Err(Error::new::<ClapError>(ClapError::raw(
            ErrorKind::ValueValidation,
            "Specified event time is in the past.",
        )));
    };

    Ok((time, mx_time))
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserRsvp {
    pub id: OwnedUserId,
    pub attending: Option<bool>,
    pub reactions: Option<Vec<String>>,
}

#[serde_as]
#[derive(Clone, Debug, Deserialize, Serialize, EventContent)]
#[ruma_event(type = "earth.flip.event", kind = MessageLike)]
pub struct FlipEventContent {
    // Synapse will return a 400 "JSON integer out of range" if we serialize
    // this hash as an integer. Therefore, save as a string.
    #[serde_as(as = "DisplayFromStr")]
    pub id: u64,
    pub generation: u8,
    pub name: Option<String>,
    pub game: String,
    pub game_url: Url,
    pub game_server: Option<String>,
    #[serde(default = "default_true")]
    pub ownership_required: bool,
    pub time: DateTime<Utc>,
    pub timezone: Tz,
    pub meeting_place: String,
    pub host: OwnedUserId,
    pub mx_time: DateTime<Utc>,
    pub mx_room: OwnedRoomId,
    pub mx_msg: OwnedEventId,
    pub mx_route: Vec<OwnedServerName>,
    pub cohosts: Option<Vec<UserRsvp>>,
    pub attendees: Option<Vec<UserRsvp>>,
    pub stream: Option<Url>,
    pub passed: bool,
    pub cancelled: bool,
    #[serde(
        flatten,
        skip_serializing_if = "Option::is_none",
        deserialize_with = "deserialize_relation"
    )]
    pub relates_to: Option<Relation<Box<FlipEventContent>>>,
}

impl FlipEventContent {
    pub fn new(
        name: Option<String>,
        game: String,
        game_url: Url,
        time_string: String,
        timezone: Tz,
        meeting_place: String,
        ev: &OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
        mx_room: OwnedRoomId,
        mx_route: Vec<OwnedServerName>,
    ) -> Result<FlipEventContent, Error> {
        let host = ev.sender.clone();

        let (time, mx_time) = parse_time(time_string, timezone, ev.origin_server_ts)?;
        let mx_msg = ev.event_id.clone();

        let id_hash = hash(format!("{mx_room}{mx_msg}").as_bytes());

        Ok(FlipEventContent {
            id: id_hash,
            generation: 1,
            name,
            game,
            game_url,
            game_server: None,
            ownership_required: true,
            time,
            timezone,
            meeting_place,
            host,
            mx_time,
            mx_room,
            mx_msg,
            mx_route,
            cohosts: None,
            attendees: None,
            stream: None,
            passed: false,
            cancelled: false,
            relates_to: None,
        })
    }

    pub fn name_or_game(&self) -> String {
        self.name.clone().unwrap_or_else(|| self.game.clone())
    }
    pub fn id_hex(&self) -> String {
        format!("{:x}", &self.id)
    }
    pub fn md_game(&self) -> String {
        format!("[{}]({})", &self.game, &self.game_url)
    }
    pub fn time_tz(&self) -> DateTime<Tz> {
        self.time.with_timezone(&self.timezone)
    }
    pub fn mx_time_tz(&self) -> DateTime<Tz> {
        self.mx_time.with_timezone(&self.timezone)
    }
    pub fn mx_room_permalink(&self) -> MatrixToUri {
        self.mx_room.matrix_to_uri_via(self.mx_route.clone())
    }
    pub fn mx_msg_permalink(&self) -> MatrixToUri {
        self.mx_room
            .matrix_to_event_uri_via(self.mx_msg.clone(), self.mx_route.clone())
    }
    pub fn is_in_past(&self) -> bool {
        self.time - Utc::now() <= Duration::zero()
    }
}

fn default_true() -> bool {
    true
}
